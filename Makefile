# Makefile
#	source:	entering source_code directory and compiling source code
#	install:mkdir directory for binary files and prepare package files
#	clean:	clean all binary files
#
# Alif Chen (Alif.Chen@moxa.com)

# source code name
MOXA_VERSION_CFG=base-system/etc/moxa-configs/moxa-version.conf
PKG_BUILDDATE ?= $(shell date +%y%m%d%H)

all: moxa-ver-cfg

moxa-ver-cfg:
	@echo "BUILDDATE=$(PKG_BUILDDATE)" >> $(MOXA_VERSION_CFG)

clean:
	sed -i '2,$$d' $(MOXA_VERSION_CFG)
